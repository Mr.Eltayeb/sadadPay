<?php
namespace ExternalServer;
/**
 * Created by PhpStorm.
 * User: Pato Code
 * Date: 6/2/2018
 * Time: 2:05 PM
 */
class EBS
{


    const Server = "https://172.16.199.1:8877/QAConsumer/";

    const PublicKey = "getPublicKey";
    const Payment = "payment";
    const Goverment = "requestGovernmentService";
    const Bill = "getBill";
    const Balance = "getBalance";
    const CardTransfer = "doCardTransfer";
    const AccountTranser = "doAccountTransfer";
}