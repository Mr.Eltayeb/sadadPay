<?php

use Illuminate\Database\Seeder;

class TopUpBillerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $zain = new \App\TopUpBiller();
        $zain->name = "Zain";
        $zain->save();
        $sudani = new \App\TopUpBiller();
        $sudani->name = "Sudani";
        $sudani->save();
        $mtn = new \App\TopUpBiller();
        $mtn->name = "MTN";
        $mtn->save();

    }
}
