<?php

use Illuminate\Database\Seeder;

class TransactionTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(\App\TransactionType::class , 1)->create();
        $top_up=new App\TransactionType();
        $top_up->name = "Top Up";
        $top_up->save();
    }
}
