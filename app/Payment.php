<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public function transaction(){
        return $this->belongsTo('App\Transaction' , 'transaction_id');
    }
    public function service(){
        return $this->belongsTo('App\Model\Merchant\MerchantServices' , 'service_id');
    }
    public function accountType(){
        return $this->belongsTo('App\Model\Account\AccountType' , 'payment_method');
    }
}
