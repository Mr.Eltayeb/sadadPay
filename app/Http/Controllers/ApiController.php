<?php

namespace App\Http\Controllers;

use App\BalanceInquiry;
use App\BalanceInquiryResponse;
use App\E15;
use App\PayTopUp;
use App\TopUp;
use App\TopUpBiller;
use App\TopUpType;
use Illuminate\Support\Facades\Hash;
use App\E15Response;
use App\E15Service;
use App\GovermentPaymentResponse;
use App\Model\Account\AccountType;
use App\Model\Account\BankAccount;
use App\Model\Account\MobileAccount;
use App\Model\Merchant\Merchant;
use App\Model\Merchant\MerchantServices;
use App\OurE15;
use App\Payment;
use App\PaymentResponse;
use App\ResetPassword;
use App\Response;
use App\Transaction;
use App\TransactionType;
use App\User;
use App\UserValidation;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use ExternalServer\EBS;
use ExternalServer\E15 as E15Server;
use Meng\AsyncSoap\Guzzle\Factory;
use Namshi\JOSE\JWT;
use PHPUnit\Util\Json;
use Webpatser\Uuid\Uuid;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class ApiController extends Controller
{
    const Server = "https://172.16.199.1:8877/QAConsumer/";

    const PublicKey = "getPublicKey";
    const Payment = "payment";
    const Goverment = "requestGovernmentService";
    const Bill = "getBill";
    const Balance = "getBalance";
    const CardTransfer = "doCardTransfer";
    const AccountTranser = "doAccountTransfer";
    const server = "http://196.29.166.229:8888/E15/GetInvoice.asmx?WSDL";
    //


    /*      This will be login        */
    public function authenticate(Request $request)
    {
        $userName = $request->json()->get("userName");
        $phone = $request->json()->get("phone");
        $password = $request->json()->get("password");
        //$credentials = $request->only("email","password");
        try {
            $user = "";
            if (isset($userName)) {
                $user = User::where("username", $userName)->where("password", Hash::make($password))->first();
            } else {
                $user = User::where("phone", $phone)->where("password", Hash::make($password))->first();
            }
            if (!$token = JWTAuth::fromUser($user)) {
                $response = array();
                $response += ["error" => true];
                $response += ["message" => "User Credential Invalid"];
                return response()->json($response, 401);
            }
        } catch (JWTException $ex) {
            $response = array();
            $response += ["error" => true];
            $response += ["message" => "Something went wrong"];
            return response()->json($response, 500);
        }
        if ($user->status == "1") {
            $response = array();
            $response += ["error" => false];
            $response += ["message" => "OK"];
            $response += ["token" => $token];
            return response()->json($response);
        } else {
            $response = array();
            $response += ["error" => true];
            $response += ["message" => "You Have To Activate Your Account First"];
            return response()->json($response);
        }

    }


    public function registration(Request $request)
    {
        if ($request->isJson()) {
            $user = $request->json();
            $fullName = $user->get("fullName");
            $userName = $user->get("userName");
            $phone = $user->get("phone");
            $password = $user->get("password");


            if (!isset($fullName)) {
                $response = array();
                $response += ["error" => true];
                $response += ["message" => "Insert fullName "];
                return response()->json($response, 200);

            }
            if (!isset($userName)) {
                $response = array();
                $response += ["error" => true];
                $response += ["message" => "Insert userName "];
                return response()->json($response, 200);
            }
            if (!isset($phone)) {
                $response = array();
                $response += ["error" => true];
                $response += ["message" => "Insert phone "];
                return response()->json($response, 200);
            }
            if (!isset($password)) {
                $response = array();
                $response += ["error" => true];
                $response += ["message" => "Insert password "];
                return response()->json($response, 200);
            }
            if (self::isUsernameFound($userName)) {
                $response = array();
                $response += ["error" => true];
                $response += ["message" => "Username Already Exists"];
                return response()->json($response, 200);
            }
            if (self::isPhoneAlreadyRegistered($phone)) {
                $response = array();
                $response += ["error" => true];
                $response += ["message" => "Phone Number Already Exists"];
                return response()->json($response, 200);
            }
            $user = new User();
            $user->username = $userName;
            $user->password = Hash::make($password);
            $user->phone = $phone;
            $user->fullName = $fullName;
            $user->save();
            $code = rand(100000, 999999);
            $validate = new UserValidation();
            $validate->phone = $phone;
            $validate->code = $code;
            $validate->save();
            self::sendSMS($phone, $code);
            $response = array();
            $response += ["error" => false];
            $response += ["message" => "activate Your Account With the code that send to You in SMS"];
            $response += ["code" => $code];
            return response()->json(["data"=>$response], 200);
        }
        else{
            $response = array();
            $response += ["error" => true];
            $response += ["message" => "Request Must be json"];
            return response()->json($response, 200);
        }
    }

    public function activate(Request $request)
    {
        $phone = $request->json()->get("phone");
        $code = $request->json()->get("code");

        if (!isset($phone)) {
            $response = array();
            $response += ["error" => true];
            $response += ["message" => "Insert phone "];
            return response()->json($response, 200);
        }

        if (!isset($code)) {
            $response = array();
            $response += ["error" => true];
            $response += ["message" => "Insert code "];
            return response()->json($response, 200);

        }
        if (!self::isPhoneAlreadyRegistered($phone)) {
            $response = array();
            $response += ["error" => true];
            $response += ["message" => "No Number Match this phone"];
            return response()->json($response, 200);
        }
        $validate = UserValidation::where("phone", $phone)->where("code", $code)->get();
        if ($validate->isNotEmpty()) {
            $user = User::where("phone", $phone)->first();
            $user->status = "1";
            $user->save();
            $token = JWTAuth::fromUser($user);
            $response = array();
            $response += ["error" => false];
            $response += ["message" => "Done"];
            $response += ["token" => $token];
            return response()->json($response, 200);
        }
        $response = array();
        $response += ["error" => true];
        $response += ["message" => "Wrong Activation Code"];
        return response()->json($response, 200);
    }

    public function resetPassword(Request $request)
    {
        $phone = $request->json()->get("phone");
        if (self::isPhoneAlreadyRegistered($phone)) {
            $code = rand(100000, 999999);
            $validate = new ResetPassword();
            $validate->phone = $phone;
            $validate->code = $code;
            $validate->save();
            self::sendSMS($phone, $code);
            $response = array();
            $response += ["error" => false];
            $response += ["message" => "Code Have Been Sended to Your Phone"];
            //$response +=["code" => $code];
            return response()->json($response, 200);
        }
        $response = array();
        $response += ["error" => true];
        $response += ["message" => "No Number Match this phone"];
        return response()->json($response, 200);
    }

    public function resetPasswordWithCode(Request $request)
    {
        $phone = $request->json()->get("phone");
        $code = $request->json()->get("code");
        $password = $request->json()->get("password");
        if (!isset($phone)) {
            $response = array();
            $response += ["error" => true];
            $response += ["message" => "Insert phone "];
            return response()->json($response, 200);
        }

        if (!isset($code)) {
            $response = array();
            $response += ["error" => true];
            $response += ["message" => "Insert code "];
            return response()->json($response, 200);

        }
        if (!isset($password)) {
            $response = array();
            $response += ["error" => true];
            $response += ["message" => "You Forget The New Password"];
            return response()->json($response, 200);

        }
        if (!self::isPhoneAlreadyRegistered($phone)) {
            $response = array();
            $response += ["error" => true];
            $response += ["message" => "No Number Match this phone"];
            return response()->json($response, 200);
        }
        $validate = ResetPassword::where("phone", $phone)->where("code", $code)->get();
        if ($validate->isNotEmpty()) {
            $user = User::where("phone", $phone)->first();
            $user->password = Hash::make($password);
            $user->save();
            $response = array();
            $response += ["error" => false];
            $response += ["message" => "Password Have been Reset"];
            return response()->json($response, 200);
        }
    }


    public function payment(Request $request)
    {


        //return response()->json(compact( 'token','user'));
        if ($request->isJson()) {
            $token = JWTAuth::parseToken();
            $user = $token->authenticate();
            //$user = JWTAuth::toUser($token);
            /******   Create Transaction Object  *********/
            $transaction = new Transaction();
            $transaction->user()->associate($user);
            $service = $request->json()->get("service");
            $service_id = $this->getServiceId($service);
            if ($service_id == null) {
                $res = array();
                $res += ["error" => true];
                $res += ["message" => "Service Name Not Found"];
                return response()->json($res, 200);
            }
            $type = TransactionType::where('name', "payment")->pluck('id')->first();
            $transaction->transactionType()->associate($type);
            $convert = $this->getDateTime();


            $uuid = Uuid::generate()->string;
            //$uuid=Uuid::randomBytes(16);

            $transaction->uuid = $uuid;
            $transaction->transDateTime = $convert;
            $transaction->status = "created";
            $transaction->save();


            //return response()->json(["id" => $uuid],200);
//            $request->merge(["UUID" => $uuid]);
//            $request->merge(["transDateTime" => $convert]);
//            $request->merge(["transaction" => $transaction]);
            /*****   Create Payment Object     ******/
            $payment = new Payment();
            $payment->transaction()->associate($transaction);

            $payment->service()->associate($service_id);
            $payment->accountType()->associate(2);
            $payment->save();


            return response()->json(["id" => $uuid], 200);
        } else
            return response("Not Json", 415);
    }

    public function payment_account(Request $request)
    {
        if ($request->isJson()) {
            $token = JWTAuth::parseToken();
            $user = $token->authenticate();
            $transaction_uuid = $request->id;
            $transaction = $this->getTransactionFromUUID($transaction_uuid);
            $payment = Payment::where("transaction_id", $transaction->id)->first();

            if ($this->isUserAuthoraized($user, $transaction)) {
                $account = $request->json()->all()["account"];
                $paymentType = $request->json()->all()["type"];
                $type = $this->getAccountTypeId($paymentType);
                //return response()->json(["type" => $type] , 200);
                $payment->accountType()->associate($type);
                $payment->save();
                $transaction->status = "Create Account";
                $transaction->save();
                $response = $this->checkAccount($paymentType, $account);
                if ($response == null) {
                    if ($paymentType == "mobile") {
                        $mobile = $account["mobile"];
                        $ipin = $account["ipin"];
                        $this->saveMobileAccount($mobile, $ipin, $transaction);
                    } else {
                        $pan = $account["pan"];
                        $ipin = $account["ipin"];
                        $expDate = $account["expDate"];
                        $mbr = $account["mbr"];
                        saveBankAccount($pan, $ipin, $expDate, $mbr, $transaction);
                    }
                    //return response()->json(["lsal" => $payment->service->id],'200');
                    $service = MerchantServices::where("id", $payment->service->id)->first();
                    if ($service->type->name == "goverment") {
                        $response = self::sendGovermentServiceRequest($transaction->id);
                        $basicResonse = self::saveBasicResponse($transaction, $response);
                        $paymentResponse = self::savePaymentResponse($basicResonse, $payment, $response);
                        self::saveGovermentResponse($paymentResponse, $response);
                        $transaction->status = "done";
                        $transaction->save();
                        return response()->json($response, '200');

                    } else if ($service->type->name == "private") {
                        $response = self::sendPaymentRequest($transaction->id);
                        $basicResonse = self::saveBasicResponse($transaction, $response);
                        $paymentResponse = self::savePaymentResponse($basicResonse, $payment, $response);
                        $transaction->status = "done";
                        $transaction->save();
                        return response()->json($response, '200');
                    }
                    $res = array();
                    $res += ["status" => "done"];
                    $res += ["transaction_id" => $transaction->uuid];
                    return response()->json($res, '200');

                } else {
                    return response()->json(["error" => "Some Field are missing"], '200');
                }

            } else {
                return response()->json(["error" => "This User Not Authorized"], 200);
            }
        }
    }

    public function topUp(Request $request)
    {
        if ($request->isJson()) {
            $token = JWTAuth::parseToken();
            $user = $token->authenticate();
            //$user = JWTAuth::toUser($token);
            /******   Create Transaction Object  *********/
            $transaction = new Transaction();
            $transaction->user()->associate($user);
            $phone = $request->json()->get("phone");
            $biller = $request->json()->get("biller");
            $amount = $request->json()->get("amount");
            $type = $request->json()->get("type");

            $paymentType = $request->json()->get("paymentType");
            $account = $request->json()->get("account");
            if (!isset($phone)) {
                $response = array();
                $response += ["error" => true];
                $response += ["message" => "Insert phone "];
                return response()->json($response, 200);

            }
            if (!isset($biller)) {
                $response = array();
                $response += ["error" => true];
                $response += ["message" => "Insert biller "];
                return response()->json($response, 200);

            }
            if (!isset($amount)) {
                $response = array();
                $response += ["error" => true];
                $response += ["message" => "Insert amount "];
                return response()->json($response, 200);

            }
            if (!isset($type)) {
                $response = array();
                $response += ["error" => true];
                $response += ["message" => "Insert type "];
                return response()->json($response, 200);

            }
            if (!isset($paymentType)) {
                $response = array();
                $response += ["error" => true];
                $response += ["message" => "Insert paymentType "];
                return response()->json($response, 200);

            }
            if (!isset($account)) {
                $response = array();
                $response += ["error" => true];
                $response += ["message" => "Insert account "];
                return response()->json($response, 200);

            }
            $transction_type = TransactionType::where('name', "Top Up")->pluck('id')->first();
            $transaction->transactionType()->associate($transction_type);
            $convert = $this->getDateTime();


            $uuid = Uuid::generate()->string;
            //$uuid=Uuid::randomBytes(16);

            $transaction->uuid = $uuid;
            $transaction->transDateTime = $convert;
            $transaction->status = "created";
            $transaction->save();

            $payment = new Payment();
            $payment->transaction()->associate($transaction);
            $payment->accountType()->associate(2);
            $payment->save();

            $account = $request->json()->all()["account"];
            $paymentType = $request->json()->all()["paymentType"];
            $AccountType = $this->getAccountTypeId($paymentType);
            //return response()->json(["type" => $type] , 200);
            $payment->accountType()->associate($AccountType);
            $payment->save();
            $transaction->status = "Create Account";
            $transaction->save();
            $response = $this->checkAccount($paymentType, $account);
            if ($response == null) {
                if ($paymentType == "mobile") {
                    $mobile = $account["mobile"];
                    $ipin = $account["ipin"];
                    $this->saveMobileAccount($mobile, $ipin, $transaction);
                } else {
                    $pan = $account["pan"];
                    $ipin = $account["ipin"];
                    $expDate = $account["expDate"];
                    $mbr = $account["mbr"];
                    saveBankAccount($pan, $ipin, $expDate, $mbr, $transaction);
                }
                $biller_id = self::getBillerId($biller);

                $type_id = self::getTopUpTypeId($type);
                //return response()->json(["type"=>$type_id],200);
                $topUp = self::getTopUp($type_id, $biller_id);
                //return response()->json(["type"=>$topUp],200);
                $pay_top_up = new PayTopUp();
                $pay_top_up->phone = $phone;
                $pay_top_up->amount = $amount;
                $pay_top_up->topUp()->associate($topUp);
                $pay_top_up->payment()->associate($payment);
                $transaction->status = "Save Top Up";
                $transaction->save();
                $pay_top_up->save();
                $paymentInfo = "MPHONE =" . $phone;
                $payee_id = $topUp->payee_id;

                $response = self::sendPaymentRequest($transaction->id, $paymentInfo, $amount, $payee_id);
                $basicResonse = self::saveBasicResponse($transaction, $response);
                $paymentResponse = self::savePaymentResponse($basicResonse, $payment, $response);
                $transaction->status = "done";
                $transaction->save();
                return response()->json($response, '200');
            } else {
                return response()->json(["error" => "Some Field are missing"], '200');
            }

        } else {
            $response = array();
            $response += ["error" => true];
            $response += ["message" => "Request Must Be Json"];
            return response()->json($response, 200);
        }
    }
    public function balance_inquiry(Request $request){
        if ($request->isJson()){
            $type = $request->json()->get("type");
            $account = $request->json()->get("account");
            if (!isset($type)){
                $response = array();
                $response += ["error" => true];
                $response += ["message" => "Send Type with the request"];
                return response()->json(["data"=>$response],200);
            }
            if (!isset($account)){
                $response = array();
                $response += ["error" => true];
                $response += ["message" => "Send Account with the request"];
                return response()->json(["data"=>$response],200);
            }
            $token = JWTAuth::parseToken();
            $user = $token->authenticate();
            //$user = JWTAuth::toUser($token);
            /******   Create Transaction Object  *********/
            $transaction = new Transaction();
            $transaction->user()->associate($user);
            $transaction_type = TransactionType::where('name', "Balance Inquiry")->pluck('id')->first();
            $transaction->transactionType()->associate($transaction_type);
            $convert = $this->getDateTime();


            $uuid = Uuid::generate()->string;
            //$uuid=Uuid::randomBytes(16);

            $transaction->uuid = $uuid;
            $transaction->transDateTime = $convert;
            $transaction->status = "created";
            $transaction->save();
            $balance_inquiry = new BalanceInquiry();
            $balance_inquiry->transaction()->associate($transaction);

            $accountType = $this->getAccountTypeId($type);
            $balance_inquiry->account_type()->associate($accountType);
            $balance_inquiry->save();
            $response = $this->checkAccount($type, $account);
            if ($response == null) {
                if ($type == "mobile") {
                    $mobile = $account["mobile"];
                    $ipin = $account["ipin"];
                    $this->saveMobileAccount($mobile, $ipin, $transaction);
                } else {
                    $pan = $account["pan"];
                    $ipin = $account["ipin"];
                    $expDate = $account["expDate"];
                    $mbr = $account["mbr"];
                    saveBankAccount($pan, $ipin, $expDate, $mbr, $transaction);
                }

            }
            else{
                return response()->json(["error" => "Some Field are missing"], '200');
            }
            $response = self::sendBalanceInquiry($transaction->id);
            $basicResonse = self::saveBasicResponse($transaction, $response);
            $balance_inquiry_reponse = self::saveBalanceInquiryResonse($basicResonse,$balance_inquiry,$response);
            return response()->json($response, '200');
        }
        else{
            $response = array();
            $response += ["error" => true];
            $response += ["message" => "Request Must Send In Json"];
            return response()->json(["data"=>$response],200);
        }
    }

    /*

    Waiting for clarifiction

    public function bill_inquiry(Request $request){
        if ($request->isJson()){

        }
        else{
            $response = array();
            $response += ["error" => true];
            $response += ["message" => "Request Must Send In Json"];
            return response()->json(["data"=>$response],200);
        }
    }

    */
    public function saveMobileAccount($mobile, $ipin, $transaction)
    {
        $mobile_account = new MobileAccount();
        $mobile_account->mobileNumber = $mobile;
        $mobile_account->IPIN = $ipin;
        $mobile_account->transaction()->associate($transaction);
        $mobile_account->save();
    }

    public function saveBankAccount($pan, $ipin, $expDate, $mbr, $transaction)
    {
        $bank_account = new BankAccount();
        $bank_account->PAN = $pan;
        $bank_account->IPIN = $ipin;
        $bank_account->expDate = $expDate;
        $bank_account->mbr = $mbr;
        $bank_account->transaction()->associate($transaction);
        $bank_account->save();
    }

    public static function getDateTime()
    {
        $now = Carbon::now();
        $day = $now->day >= 10 ? $now->day : "0" . $now->day;
        $mounth = $now->month >= 10 ? $now->month : "0" . $now->month;
        $year = $now->year - 2000;
        $hour = $now->hour >= 10 ? $now->hour : "0" . $now->hour;
        $minute = $now->minute >= 10 ? $now->minute : "0" . $now->minute;
        $second = $now->second >= 10 ? $now->second : "0" . $now->second;
        $convert = "" . $day . "" . $mounth . "" . $year . "" . $hour . "" . $minute . "" . $second . "";
        return $convert;
    }

    public function convertDateTime($nw)
    {
        $now = Carbon::parse($nw);
        /////////////// Convert DateTime Carbon
        //$now=Carbon::create($nw);
        $day = $now->day >= 10 ? $now->day : "0" . $now->day;
        $mounth = $now->month >= 10 ? $now->month : "0" . $now->month;
        $year = $now->year - 2000;
        $hour = $now->hour >= 10 ? $now->hour : "0" . $now->hour;
        $minute = $now->minute >= 10 ? $now->minute : "0" . $now->minute;
        $second = $now->second >= 10 ? $now->second : "0" . $now->second;
        $convert = "" . $day . "" . $mounth . "" . $year . "" . $hour . "" . $minute . "" . $second . "";
        return $convert;
    }


    public function checkAccount($paymentType, $account)
    {
        if ($paymentType == "mobile") {
            if (isset($account["mobile"]) && isset($account["ipin"])) {
                return null;
            } else {
                return response()->json(
                    [
                        "error" => "There are some error",
                        "status" => "mobile account missing"
                    ]
                    , '200');
            }
        } else {
            if (isset($account["pan"]) && isset($account["ipin"]) && isset($account["expDate"]) && isset($account["mbr"])) {
                return null;
            } else {
                return response()->json(
                    [
                        "error" => "There are some error",
                        "status" => "bank account missing"
                    ]
                    , '200');
            }
        }
    }


    public function getServiceId($name)
    {
        return MerchantServices::where('name', $name)->first();
    }

    public function isUserAuthoraized($user, $transaction)
    {
        return Transaction::where("id", $transaction->id)->where("user_id", $user->id)->first();
    }

    public function isUsernameFound($username)
    {
        $user = User::where("username", $username)->get();
        return $user->isEmpty() ? false : true;
    }

    public function getTransactionFromUUID($uuid)
    {
        return Transaction::where('uuid', $uuid)->first();
    }

    public function getAccountTypeId($account)
    {
        return AccountType::where("name", $account)->first();
    }

    public static function requestBuild($transaction_id, $payment_info = "", $topUpAmount = "", $payee_id = "")
    {
        $transaction = Transaction::where("id", $transaction_id)->first();
        $payment = Payment::where("transaction_id", $transaction_id)->first();
        if ($payment_info == "") {
            $service = MerchantServices::where("id", $payment->service->id)->first();
            $merchant = Merchant::where("id", $service->merchant->id)->first();
        }

        //return ["id" => $payment->service];
        $tranCurrency = "SDG";
        $tranAmount = "";
        if ($payment_info == "") {
            $tranAmount = $service->totalFees;
        } else {
            $tranAmount = $topUpAmount;
        }
        $request = array();
        $request += ["applicationId" => "sadad"];
        //$tr = array();
        //$tr += ["tranDateTime" => $transaction->transDateTime];

        //return $tr;

        $transDateTime = $transaction->transDateTime;

        $request += ["tranDateTime" => $transDateTime];
        $uuid = $transaction->uuid;
        $request += ["UUID" => $uuid];
        $request += ["tranCurrency" => $tranCurrency];
        $request += ["tranAmount" => $tranAmount];
        $account_type = $payment->accountType->name;
        $userName = "";
        $userPassword = "";
        $entityId = "";
        $entityType = "";
        $PAN = "";
        $mbr = "";
        $expDate = "";
        $IPIN = "";
        $authenticationType = "00";
        if ($account_type == "mobile") {
            $mobile = MobileAccount::where("transaction_id", $transaction_id)->first();
            $authenticationType = "10";
            $entityId = $mobile->mobileNumber;
            $entityType = "Phone No";
            $IPIN = $mobile->IPIN;
            $publicKey = self::sendPublicKey();
            $IPIN = base64_encode(self::encript($publicKey, $uuid, $IPIN));
        } else {
            $bank = BankAccount::where("transaction_id", $transaction_id)->first();
            $PAN = $bank->PAN;
            $mbr = $bank->mbr;
            $expDate = $bank->expDate;
            $IPIN = $bank->IPIN;
            $publicKey = self::sendPublicKey();
            $IPIN = base64_encode(self::encript($publicKey, $uuid, $IPIN));
        }
        if ($payee_id != "") {
            $request += ["payeeId" => $payee_id];
        } else {
            $request += ["payeeId" => $merchant->payee_id];
        }
        $request += ["paymentInfo" => $payment_info];

        $request += ["userName" => $userName];
        $request += ["userPassword" => $userPassword];
        $request += ["entityId" => $entityId];
        $request += ["entityType" => $entityType];
        $request += ["PAN" => $PAN];

        $request += ["mbr" => $mbr];
        $request += ["expDate" => $expDate];
        $request += ["IPIN" => $IPIN];
        $request += ["authenticationType" => $authenticationType];
        $request += ["fromAccountType" => "00"];
        return $request;
        //$request->tranDateTime = $transaction->transDateTime;
    }

    public static function buildPublickKey()
    {
        $tranDateTime = self::getDateTime();
        $uuid = Uuid::generate()->string;
        $applicationId = "Sadad";
        $request = array();
        $request += ["applicationId" => $applicationId];
        $request += ["UUID" => $uuid];
        $request += ["tranDateTime" => $tranDateTime];
        return $request;
    }

    public static function sendPublicKey()
    {
        $req = self::buildPublickKey();
        $req_json = \GuzzleHttp\json_encode($req);
        $client = new Client([
            'base_uri' => self::Server + self::PublicKey,
        ]);
        $response = $client->post(self::Server + self::PublicKey, [
            'debug' => TRUE,
            'body' => $req_json,
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);
        $body = $response->getBody();
        return $body->pubKeyValue;
    }

    public static function BuildE15($transaction_id)
    {
        $request = array();
        $transaction = Transaction::where("id", $transaction_id)->first();
        $payment = Payment::where("transaction_id", $transaction_id)->first();
        $e15 = E15::where("payment_id", $payment->id);
        $service = MerchantServices::where("id", $payment->service->id)->first();

        $e15Service = E15Service::where('service_id', $service->id)->first();

        $ourE15 = OurE15::all()->first();
        $credential = array();
        $E15Request = array();
        $payment_details = array();
        $payment_details += ["id" => $e15Service->e15_id];
        $payment_details += ["amount" => $service->totalFees];
        $credential += ["userName" => $ourE15->userName];
        $credential += ["password" => $ourE15->password];
        $credential += ["appId" => $ourE15->appId];
        $credential += ["key" => $ourE15->key];

        $E15Request += ["phone" => $e15->phone];
        $E15Request += ["ref_id" => $e15Service->ref_id];
        $E15Request += ["terminal_id" => $e15Service->terminal_id];
        $E15Request += ["payer_name" => $e15->name];
        $E15Request += ["identity" => $e15->identity];
        $E15Request += ["identity_type" => $e15->identity_type];
        $E15Request += ["description" => $e15->description];
        $E15Request += ["payment_details" => array($payment_details)];
        $E15Request += ["payment_method_id" => 3];
        $E15Request += ["total_amount" => $service->totalFees];
        $E15Request += ["currency_id" => 1];
        $E15Request += ["discount" => 1];
        $E15Request += ["has_vat" => $e15Service->has_vat];
        $E15Request += ["uuid" => $transaction->uuid];
        $request += ["Credential" => $credential];
        $request += ["E15Request" => $E15Request];
        return $request;

    }

    public static function sendE15($transaction_id)
    {
        $transaction = Transaction::where("id", $transaction_id)->first();
        $transaction->status = "To E15";
        $transaction->save();
        $req = self::BuildE15($transaction_id);
        $req = \GuzzleHttp\json_encode($req);
        $factory = new Factory();
        $client = $factory->create(new Client(), self::server);
        $response = $client->call('request_receipt', $req);
        return $response;
    }

    public static function BuildRequestGovServices($transaction_id)
    {
        $request = array();
        $transaction = Transaction::where("id", $transaction_id)->first();
        $payment = Payment::where("transaction_id", $transaction_id)->first();

        $service = MerchantServices::where("id", $payment->service->id)->first();
        $merchant = Merchant::where("id", $service->merchant->id)->first();
        $e15 = E15Service::where('service_id', $service->id);
        $e15Service = E15Service::where('service_id', $service->id)->first();
        $e15Response = E15Response::where('e15_id', $e15->id);
        $request += ["applicationId", "Sadad"];
        $request += ["tranDateTime", $transaction->transDateTime];
        $uuid = $transaction->uuid;
        $request += ["UUID", $uuid];
        $account_type = $payment->accountType->name;
        $userName = "";
        $userPassword = "";
        $entityId = "";
        $entityType = "";
        $PAN = "";
        $mbr = "";
        $expDate = "";
        $IPIN = "";
        $authenticationType = "00";
        if ($account_type == "mobile") {
            $mobile = MobileAccount::where("transaction_id", $transaction_id)->first();
            $authenticationType = "10";
            $entityId = $mobile->mobileNumber;
            $entityType = "Phone No";
            $IPIN = $mobile->IPIN;
            $publicKey = self::sendPublicKey();
            $IPIN = base64_encode(self::encript($publicKey, $uuid, $IPIN));
        } else {
            $bank = BankAccount::where("transaction_id", $transaction_id)->first();
            $PAN = $bank->PAN;

            $mbr = $bank->mbr;
            $expDate = $bank->expDate;
            $IPIN = $bank->IPIN;
            $publicKey = self::sendPublicKey();
            $IPIN = base64_encode(self::encript($publicKey, $uuid, $IPIN));
        }
        $request += ["userName" => $userName];
        $request += ["userPassword" => $userPassword];
        $request += ["entityId" => $entityId];
        $request += ["entityType" => $entityType];
        $request += ["PAN" => $PAN];

        $request += ["mbr" => $mbr];
        $request += ["expDate" => $expDate];
        $request += ["IPIN" => $IPIN];
        $request += ["authenticationType" => $authenticationType];
        $request += ["fromAccountType" => "00"];


        $tranCurrency = "SDG";
        $tranAmount = $service->totalFees;
        $request += ["tranCurrency" => $tranCurrency];
        $request += ["tranAmount" => $tranAmount];
        $request += ["payeeId" => $merchant->payee_id];
        $request += ["requestedServicesId" => $e15Service->e15_id];
        $request += ["phone" => $e15->phone];
        $request += ["referenceId" => 1];
        $request += ["terminalId" => $e15Service->terminal_id];
        //$request += ["payerame" => $e15->name];
        $request += ["identity" => $e15->identity];
        $request += ["identity_type" => $e15->identity_type];
        $request += ["description" => $e15->description];
        $payment_details = array();
        $payment_details += ["id" => $e15Service->e15_id];
        $payment_details += ["amount" => $service->totalFees];
        $request += ["payment_details" => array($payment_details)];
        $request += ["payment_method_id" => 3];
        $request += ["total_amount" => $service->totalFees];
        $request += ["discount" => 1];
        $request += ["hasVat" => $e15Service->has_vat];
        $request += ["invoiceNo" => $e15Response->invoice_id];
        return $request;

    }

    public static function BuildRequestBalanceInquiry($transaction_id){
        $request = array();
        $transaction = Transaction::where("id", $transaction_id)->first();
        $balance_inquiry = BalanceInquiry::where("transaction_id", $transaction_id)->first();
        $request += ["applicationId", "Sadad"];
        $request += ["tranDateTime", $transaction->transDateTime];
        $uuid = $transaction->uuid;
        $request += ["UUID", $uuid];
        $account_type = $balance_inquiry->account_type->name;
        $userName = "";
        $userPassword = "";
        $entityId = "";
        $entityType = "";
        $PAN = "";
        $mbr = "";
        $expDate = "";
        $IPIN = "";
        $authenticationType = "00";
        if ($account_type == "mobile") {
            $mobile = MobileAccount::where("transaction_id", $transaction_id)->first();
            $authenticationType = "10";
            $entityId = $mobile->mobileNumber;
            $entityType = "Phone No";
            $IPIN = $mobile->IPIN;
            $publicKey = self::sendPublicKey();
            $IPIN = base64_encode(self::encript($publicKey, $uuid, $IPIN));
        } else {
            $bank = BankAccount::where("transaction_id", $transaction_id)->first();
            $PAN = $bank->PAN;

            $mbr = $bank->mbr;
            $expDate = $bank->expDate;
            $IPIN = $bank->IPIN;
            $publicKey = self::sendPublicKey();
            $IPIN = base64_encode(self::encript($publicKey, $uuid, $IPIN));
        }
        $request += ["userName" => $userName];
        $request += ["userPassword" => $userPassword];
        $request += ["entityId" => $entityId];
        $request += ["entityType" => $entityType];
        $request += ["PAN" => $PAN];

        $request += ["mbr" => $mbr];
        $request += ["expDate" => $expDate];
        $request += ["IPIN" => $IPIN];
        $request += ["authenticationType" => $authenticationType];
        $request += ["fromAccountType" => "00"];


        $tranCurrency = "SDG";
        $request += ["tranCurrency" => $tranCurrency];
        return $request;

    }

    public static function sendBalanceInquiry($transaction_id){
        $transaction = Transaction::where("id", $transaction_id)->first();
        $transaction->status = "To EBS";
        $transaction->save();
        $req = self::BuildRequestBalanceInquiry($transaction_id);
        $req_json = \GuzzleHttp\json_encode($req);
        $client = new Client([
            'base_uri' => self::Server + self::Balance,
        ]);
        $response = $client->post(self::Server + self::Balance, [
            'debug' => TRUE,
            'body' => $req_json,
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);
        return $response->getBody();
    }

    public static function getPublicKey()
    {
        $req = self::buildPublickKey();
        $req = \GuzzleHttp\json_encode($req);
        $client = new Client();

        $response = $client->request('POST', self::Server + self::Goverment, array(
            'content-type' => 'application/json'
        ), $req)->send();

        //$response=$client->request('GET', 'http://www.google.com');

//        $request->setBody($req);
        //$response = $request->send();
        return $response;
    }

    public static function saveE15Response($transaction_id)
    {
        //$transaction = Transaction::where("id",$transaction_id)->first();
        $payment = Payment::where("transaction_id", $transaction_id)->first();
        $e15 = E15::where("payment_id", $payment->id);
        $response = self::sendE15($transaction_id);
        $e15response = new E15Response();
        $e15response->E15()->associate($e15);
        $e15response->invoice_no = $response->invoice_no;
        $e15response->expiry = $response->expiry;
        $e15response->response_code = $response->response_code;
        $e15response->response_message = $response->response_message;
        $e15response->save();
    }

    public static function sendPaymentRequest($transaction_id, $payment_info = "", $topUpAmount = "", $payee_id = "")
    {
        $transaction = Transaction::where("id", $transaction_id)->first();
        $transaction->status = "To EBS";
        $transaction->save();
        $req = self::requestBuild($transaction_id, $payment_info, $topUpAmount, $payee_id);

        $req_json = \GuzzleHttp\json_encode($req);
        $client = new Client([
            'base_uri' => self::Server + self::Payment,
        ]);
        $response = $client->post(self::Server + self::Payment, [
            'debug' => TRUE,
            'body' => $req_json,
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);
        return $response->getBody();
    }

    public static function sendGovermentServiceRequest($transaction_id)
    {
        self::saveE15Response($transaction_id);
        $req = self::BuildRequestGovServices($transaction_id);
        $req = \GuzzleHttp\json_encode($req);
        $req_json = \GuzzleHttp\json_encode($req);
        $client = new Client([
            'base_uri' => self::Server + self::Goverment,
        ]);
        $response = $client->post(self::Server + self::Goverment, [
            'debug' => TRUE,
            'body' => $req_json,
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);
        return $response->getBody();
    }

    public static function saveBasicResponse($transaction, $response)
    {
        $basicResonse = new Response();
        $basicResonse->transaction()->associate($transaction);
        $basicResonse->responseCode = $response->responseCode;
        $basicResonse->responseMessage = $response->responseMessage;
        $basicResonse->responseStatus = $response->responseStatus;
        $basicResonse->save();
        return $basicResonse;
    }

    public static function savePaymentResponse($basicResonse, $payment, $response)
    {
        $paymentResponse = new PaymentResponse();
        $paymentResponse->response()->associate($basicResonse);
        $paymentResponse->payment()->associate($payment);
        $paymentResponse->balance = $response->balance;
        $paymentResponse->acqTranFee = $response->acqTranFee;
        $paymentResponse->issuerTranFee = $response->issuerTranFee;
        $paymentResponse->billInfo = $response->billInfo != "" ? $response->billInfo : "";
        $paymentResponse->save();
        return $paymentResponse;
    }

    public static function saveGovermentResponse($paymentResponse, $response)
    {
        $govermentResponse = new GovermentPaymentResponse();
        $govermentResponse->paymentResponse()->associate($paymentResponse);
        $govermentResponse->invoiceExpiryDate = $response->invoiceExpiryDate;
        $govermentResponse->invoiceStatus = $response->invoiceStatus;
        $govermentResponse->reciptNo = $response->reciptNo;
        $govermentResponse->unitName = $response->unitName;
        $govermentResponse->serviceName = $response->serviceName;
        $govermentResponse->totalAmountInt = $response->totalAmountInt;
        $govermentResponse->totalAmountInWord = $response->totalAmountInWord;
        $govermentResponse->amountDue = $response->amountDue;
        $govermentResponse->availableBalance = $response->availableBalance;
        $govermentResponse->legerBalance = $response->legerBalance;
        $govermentResponse->tranFee = $response->tranFee;
        $govermentResponse->save();
        return $govermentResponse;
    }
    public static function saveBalanceInquiryResonse($basicResonse , $balance_inquiry,$response){
        $balance_inquiry_response = new BalanceInquiryResponse();
        $balance_inquiry_response->response()->associate($basicResonse);
        $balance_inquiry_response->balanceInquiry()->associate($balance_inquiry);
        $balance_inquiry_response->balance = $response->balance;
        $balance_inquiry_response->acqTranFee = $response->acqTranFee;
        $balance_inquiry_response->issuerTranFee = $response->issuerTranFee;
        $balance_inquiry_response->issuerTranFee = $response->issuerTranFee;
        $balance_inquiry_response->save();
        return $balance_inquiry_response;

    }

    public static function encript($publicKey, $uuid, $ipin)
    {
        $rsa = new Crypt_RSA();
        $rsa->loadKey($publicKey);
        $rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
        $ciphertext = $rsa->encrypt($uuid + $ipin);
        return $ciphertext;
    }

    public static function isPhoneAlreadyRegistered($phone)
    {
        $user = User::where("phone", $phone)->get();
        return $user->isEmpty() ? false : true;
    }

    public static function sendSMS($phone, $code)
    {
        $service_url = 'http://sms.iprosolution-sd.com/app/gateway/gateway.php'; //'http://api.unifonic.com/rest/Messages/Send';
        $curl = curl_init($service_url);
        $curl_post_data = array(
            "sendmessage" => 1,
            "username" => 'Sadad',
            "password" => 'Sadad@123',
            "text" => 'مرحباً بك في تطبيق بروبرتيز ، رمز التحقق هو  ' . $code,
            "numbers" => $phone,
            "sender" => 'Properties',
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        $curl_response = curl_exec($curl);
        curl_close($curl);
    }

    public static function getBillerId($biller)
    {
        return TopUpBiller::where('name', $biller)->pluck('id')->first();
    }

    public static function getTopUpTypeId($type)
    {
        return TopUpType::where('name', $type)->pluck('id')->first();
    }

    public static function getTopUp($type_id, $biller_id)
    {
        return TopUp::where('type_id', $type_id)->where('biller_id', $biller_id)->first();
    }
}
