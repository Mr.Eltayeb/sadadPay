<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;

class API
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->has('token')) {
            try {
                $this->auth = JWTAuth::parseToken()->authenticate();
                return $next($request);
            } catch (JWTException $e) {
                $response = array();
                $response += ["error" => true];
                $response += ["message" => "Authentication Error"];
                return response()->json($response,"200");
            }
        }
    }
}
