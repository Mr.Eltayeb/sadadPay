<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Tymon\JWTAuth;

class User extends Authenticatable
{
    use HasApiTokens  , Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function transactions(){
        return $this->hasMany('App\Transaction');
    }
    public function userGroup(){
        return $this->belongsTo('App\UserGroup');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    protected $fillable = [
        'name', 'email', 'password','username','fullName'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
