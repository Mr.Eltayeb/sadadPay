<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GovermentPaymentResponse extends Model
{
    //
    public function paymentResponse(){
        return $this->belongsTo('App\PaymentResponse','payment_response_id');
    }
}
