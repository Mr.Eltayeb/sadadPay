<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopUp extends Model
{
    //
    public function type(){
        return $this->belongsTo('App\TopUpType' , 'type_id');
    }
    public function biller(){
        return $this->belongsTo('App\TopUpBiller' , 'biller_id');
    }
}
