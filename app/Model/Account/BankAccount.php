<?php

namespace App\Model\Account;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    //
    public function transaction(){
        return $this->belongsTo('App\Transaction',"transaction_id");
    }
}
