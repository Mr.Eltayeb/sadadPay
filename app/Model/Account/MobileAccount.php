<?php

namespace App\Model\Account;

use Illuminate\Database\Eloquent\Model;

class MobileAccount extends Model
{
    //
    public function transaction(){
        return $this->belongsTo('App\Transaction',"transaction_id");
    }
}
