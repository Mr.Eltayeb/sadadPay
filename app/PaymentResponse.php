<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentResponse extends Model
{
    //
    public function payment(){
        return $this->belongsTo('App\Payment' , 'payment_id');
    }
    public function response(){
        return $this->belongsTo('App\Response' , 'response_id');
    }
}
