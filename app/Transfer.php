<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    //
    public function transaction(){
        return $this->belongsTo('App\Response');
    }
    public function type(){
        return $this->belongsTo('App\TransferType');
    }
}
