<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayTopUp extends Model
{
    //
    public function topUp(){
        return $this->belongsTo('App\TopUp','top_up_id');
    }
    public function payment(){
        return $this->belongsTo('App\Payment','payment_id');
    }
}
