<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BalanceInquiry extends Model
{
    //
    public function transaction(){
        return $this->belongsTo('App\Transaction' , "transaction_id");
    }
    public function account_type(){
        return $this->belongsTo('App\Model\Account\AccountType' , "account_type_id");
    }
}
