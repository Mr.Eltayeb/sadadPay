<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillInquiryResponse extends Model
{
    //
    public function billInquiry(){
        return $this->belongsTo('App\BillInquiry');
    }
}
