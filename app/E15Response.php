<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class E15Response extends Model
{
    //
    public function E15(){
        return $this->belongsTo('App\E15');
    }
}
