<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferResponse extends Model
{
    public function trasfer(){
        return $this->belongsTo('App\Transfer');
    }
    public function response(){
        return $this->belongsTo('App\Response');
    }
}
