<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BalanceInquiryResponse extends Model
{
    //
    public function balanceInquiry(){
        return $this->belongsTo('App\BalanceInquiry');
    }
    public function response(){
        return $this->belongsTo('App\Response');
    }
}
