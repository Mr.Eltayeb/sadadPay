<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
    public function user(){
        return $this->belongsTo('App\User' , 'user_id');

    }
    public function transactionType(){
        return $this->belongsTo('App\TransactionType' , 'transaction_type');
    }
    public function payment(){
        return $this->hasOne('App\Payment');
    }
    public function topUp(){
        return $this->hasOne('App\TopUp');
    }
    public function balanceInquiry(){
        return $this->hasOne('App\BalanceInquiry');
    }
}
