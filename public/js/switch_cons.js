/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function telecome(show) {
    $("#select_op").addClass("hide");
    $("#operations").removeClass("hide");
    $("#" + show).removeClass("hide");
}
function getServices() {
    var id = $('#biller').val();
    var x = '';
    $('#service_div').html("");
    $('#ajax-loader').removeClass('hide');
    $.get("ServicePayment?key=serv&id=" + id, function (data, status) {
        if (status == 'success') {
            $('#ajax-loader').addClass('hide');
            $('#service_div').append('<label class="control-label" for="tel_service">Chose Service</label>');
            for (i = 0; i < data.length; i++) {
                if (i == 0) {
                    x = 'checked="checked"';
                } else {
                    x = '';
                }
                $('#service_div').append('<div class="radio"><label><input type="radio" name="service" id="tel_service_' + data[i].id + '" value="' + data[i].id + '" ' + x + '>' + data[i].name + '</label></div>');
            }
            $('#service_div').append('<div class="help-block with-errors"></div>');
        }
    });
}
function home(show) {
    $("#select_op").removeClass("hide");
    $("#operations").addClass("hide");
    $('#' + show).addClass("hide");
}
function higher(show) {
    if (show == 8) {
        $("#arab").addClass("hide");
        $("#sudan").removeClass("hide");
        $("#choose").val("sudan");
        $('#setnumber').val('');
        $('#namedu').val('Mohammed Mohammed Mohammed');
        $('#phonearab').val('123456789');

    } else if (show == 9) {
        $("#sudan").addClass("hide");
        $("#arab").removeClass("hide");
        $("#choose").val("arab");
        $('#setnumber').val('123456');
         $('#namedu').val('');
        $('#phonearab').val('');
    }
}
$('#tele-form').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {
        return false;
    } else {
        return true;
    }
});

$('#nec-form').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {
        return false;
    } else {
        return true;
    }
});

$('#cus-form').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {
        return false;
    } else {
        //$.isNumeric(('#amountcus'));
        return true;
    }
});

$('#edu-form').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {
        return false;
    } else {
        return true;
    }
});

$('#gov-form').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {
        return false;
    } else {
        return true;
    }
});



